import React, { Component } from 'react'
import { View, Text, Button, StyleSheet, Alert } from 'react-native'

class Screen2 extends Component {
    state = {
        user: '',
        firstname: '-',
        lastname: '-'
    }
    UNSAFE_componentWillMount() {
        console.log(';;;;;;', this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.user) {
            console.log('screen2', this.props.location.state.user)
            this.setState({ user: this.props.location.state.user })

        } else {
            console.log('screen2', this.props)
            this.setState({ user: 'user' })
        }

    }

    goToScreen1 = () => {
        this.props.history.push('/screen1')         //history keep tract each page
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={styles.row1}>
                    <Button styles={styles.backButton} title="< BACK" onPress={this.goToScreen1} />
                    <Text style={styles.headerText}>My Profile</Text>
                </View>

                <View style={styles.row2}>
                    <View style={styles.column}>
                        <View style={styles.section}>
                            <Text style={styles.bodyText}>Username</Text>
                            <Text style={styles.subbodyText}>{this.state.user}</Text>
                        </View>
                        <View style={styles.section}>
                            <Text style={styles.bodyText}>Firstname</Text>
                            <Text style={styles.subbodyText}>{this.state.firstname}</Text>
                        </View>
                        <View style={styles.section}>
                            <Text style={styles.bodyText}>Lastname</Text>
                            <Text style={styles.subbodyText}>{this.state.lastname}</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.row3}>
                    <View style={styles.buttonView}>
                        <Button title='Edit'></Button>

                    </View>

                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    backButton: {
        margin: '2%'
    },
    row1: {
        flexDirection: 'row',
        backgroundColor: 'green',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'

    },
    row2: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        margin: '10%'
    },
    headerText: {
        fontSize: 20,
        color: 'white',
        marginLeft: '25%',
        marginRight: '25%',

    },
    bodyText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'black',

    },
    column: {
        flex: 1,
        flexDirection: 'column',

    },
    section: {
        marginBottom: 40
    },
    subbodyText: {
        fontSize: 20,
        color: 'black',
        marginLeft: 10
    },
    row3: {
        flex: 0,
        flexDirection: 'row',
        // backgroundColor: 'red',
        justifyContent: 'center',
        // alignItems: 'center'
    },
    buttonView:{
        width: '100%'
    }

})

export default Screen2