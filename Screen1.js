import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import { Link } from 'react-router-native'

class Screen1 extends Component {
    state = {
        username: '',
        password: '',
        links: ''
    }
    UNSAFE_componentWillMount() {    //The first life cycle will = จะ UNSAFE = new version
        console.log(this.props)    // if in location have state and in state have my number so alert  and alert must be string 
        // if(this.props.location && this.props.location.state && this.props.location.state.mynumber){
        //     Alert.alert('Your number is ', this.props.location.state.mynumber + '') //
        // }
    }
    checkCorrectPass = () => {
    
        let user = this.state.username.toLowerCase();
        let pass = this.state.password.toLowerCase()
        if (user === 'myusername' && pass === '1234') {
            // this.setState({links : '/screen2'})
            console.log(user)
            this.props.history.push('/screen2',{user: user})


        } else {
           
            this.props.history.push('/screen1')

        }
    }
    render() {
        return (
            <View style={[styles.container]}>
                <View style={styles.row}>
                    <View style={[styles.pictureContainer]}>
                        <Image style={styles.img} resizeMode="stretch" source={{
                            uri: 'https://myanimelist.cdn-dena.com/s/common/uploaded_files/1453139441-683781d8b6d00fea080d243b22346a95.jpeg'
                        }} />
                    </View>
                </View>
                <View style={styles.row2}>
                    <View style={styles.column}>
                        <TextInput placeholder='username' value={this.state.username} style={styles.input}
                            onChangeText={(username) => this.setState({ username })} ></TextInput>
                        <TextInput placeholder='password' value={this.state.password} style={styles.input}
                            onChangeText={(password) => this.setState({ password })} ></TextInput>
                        {/* <Link  to='/screen2'style={styles.link} onPress={this.checkCorrectPass}>
                        <Text style={styles.linktext}>Login</Text>
                    </Link> */}

                        <TouchableOpacity style={styles.link} onPress={this.checkCorrectPass}>
                            <Text style={styles.linktext}>Login</Text>
                        </TouchableOpacity>
                    </View>


                </View>
                {/* <View style={styles.row3}>
              
                </View> */}
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'grey',
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        // backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    row2: {
        flex: 1,
        flexDirection: 'row',
        // backgroundColor: 'blue',
        justifyContent: 'center',
        // alignItems: 'center'
    },
    row3: {
        flex: 0,
        flexDirection: 'row',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    pictureContainer: {
        width: 200,
        height: 150
    },
    img: {
        width: '100%',
        height: '100%'
    },
    input: {
        width: '80%',
        height: 40,
        backgroundColor: 'white',
        margin: '2%'
    },
    column: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center'
    },
    link: {
        backgroundColor: 'white',
        width: '80%',
        marginTop: '20%'

    },
    linktext: {
        fontSize: 18,
        color: 'black',
        padding: '2%',
        textAlign: 'center'
    }


})

export default Screen1